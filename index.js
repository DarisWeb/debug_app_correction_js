var correctAnswer = 'Douze'; // Réponse attendue
//Erreur false ne doit pas etre entre les ''
var answered = false; // Booléen qui permet de déterminer si la question a été répondue.

var answersList = document.querySelectorAll('.answers li'); // Liste d'élements DOM représentant les réponses possibles
var validateAnswerButton = document.querySelector('.validate'); // Element DOM représentant le bouton de validation de réponse
//Erreur il faut utiliser querySelector et non querySelectorAll
var resetButton = document.querySelector('.reset'); // Element DOM représentant le bouton de nettoyage de l'exercice
var resultField = document.querySelector('.result p'); // Element DOM représentant le champs texte dans laquelle le statut de la réponse sera affiché

/**
 * Sélectionne une réponse
 * @param {Element} selectedAnswer - Element DOM sélectionné comme réponse
 */
var selectAnswer = function(selectedAnswer) {
  if(!answered){
    //Erreur each au lie de forEach
    answersList.forEach((answer) => {

      if(answer != selectedAnswer){
        answer.classList.remove('selected');
      }

      answer.classList.remove('correct', 'incorrect');
    });
    //Erreur classList au lieu de class
    selectedAnswer.classList.toggle('selected');
  }
}

/**
 * Determine quelle réponse a été sélectionnée
 * @returns {Element} - Element DOM sélectionné comme réponse
 */
var getSelectedAnswer = function() {
  var selectedAnswer = document.createElement('div');
  answersList.forEach((answer) => {
    if(answer.classList.contains('selected')) {
      selectedAnswer = answer;
    }
  });
  return selectedAnswer;
}

/**
 * Valide la réponse et donne le résultat
 */
var validateAnswer = function() {
  if(!answered){
    var selectedAnswer = getSelectedAnswer();
    //Erreur pas de {
    switch(selectedAnswer.innerHTML){
      case '':
        resultField.innerHTML = 'Aucune réponse selectionnée';
        break;
      case correctAnswer:
        //Erreur innerHtml au lieu de innerHTML
        resultField.innerHTML = 'Bravo';
        selectedAnswer.classList.add('correct');
        break;
      default:
        resultField.innerHTML = 'Mauvaise réponse';
        selectedAnswer.classList.add('incorrect');
    //Erreur pas de }
    }
    answered = true;
  }
}

/**
 * Purge la réponse pour le recommencer
 */
var resetAnswers = function () {
  if(answered){
    //Erreur il manque =>
    answersList.forEach((answer) => {
      answer.classList.remove('selected', 'correct', 'incorrect');
    });
    resultField.innerHTML = '';
    answered = false;
  }
}

/**
 * Ajoute un écouteur d'événements clics sur les différents éléments HTML utilisés pour cet exercice 
 */
answersList.forEach((answer) => {
  //Erreur click doit être entre les ''
  answer.addEventListener('click', () => { selectAnswer(answer) })
});

validateAnswerButton.addEventListener('click', validateAnswer);

resetButton.addEventListener('click', resetAnswers);